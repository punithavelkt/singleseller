<?php
namespace Emc\Singleseller\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Directory\Model\RegionFactory;

class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;
	
	const COOKIE_NAME = 'emc_singleseller_address_cookie';
	/**
	* @var \Magento\Framework\Stdlib\CookieManagerInterface
	*/
	protected $_cookieManager;
	/**
	* @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
	*/
	protected $_cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $resultForwardFactory;
	
	protected $country;
	
	private $regionFactory;
	
	protected $_sessionManager;


    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManager $storeManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
		\Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
		\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
		\Magento\Directory\Model\Country $country,
		RegionFactory $regionFactory,
		\Magento\Framework\Session\SessionManagerInterface $sessionManager
        ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
		$this->_cookieManager = $cookieManager;
		$this->_cookieMetadataFactory = $cookieMetadataFactory;
		$this->country = $country;
		$this->regionFactory = $regionFactory;
		$this->_sessionManager = $sessionManager;
        parent::__construct($context);
    }


    public function execute()
    {
		$postData = $this->getRequest()->getParams();
		$regionId = '';
		$regionList = [];
		if(isset($postData['country']) && $postData['country'] != '') {
			$regionList = $this->getRegionsOfCountry($postData['country']);
			if(is_array($regionList) && count($regionList) > 0) {
				foreach($regionList as $key => $value) {
					if(strtolower($value['title']) == strtolower($postData['region'])) {
						$postData['region_id'] = $value['value'];
					}
				}
			}
		}
		$metadata = $this->_cookieMetadataFactory
         ->createPublicCookieMetadata()
         ->setDurationOneYear()
		 ->setPath($this->_sessionManager->getCookiePath())
		 ->setDomain($this->_sessionManager->getCookieDomain())
		 ->setHttpOnly(false);
		$this->_cookieManager->setPublicCookie(
			 self::COOKIE_NAME,
			 json_encode($postData),
			 $metadata
		);
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
		$firstname = $lastname = $street = $postal = $city = $region = $country = "";
		$addressData = $cookieArray = [];
		$quote = $cart->getQuote();
		// This will return the current quote
		$quoteId = $quote->getId();
		if($quoteId) {
			if(isset($postData) && is_array($postData) && count($postData) > 0) {
				$street = $postData['street'];
				$postal = $postData['postal'];
				$city   = $postData['city'];
				$region = $postData['region'];
				$regionId = $postData['region_id'];
				$country = $postData['country'];
				$addressData = [
					"street"     => $street,
					"city"       => $city,
					"postcode"   => $postal,
					"country_id" => $country,
					"region"	=> $region,
					"region_id" => $regionId
				];
				$quote->getShippingAddress()->addData($addressData);
				$quote->save();
			}
		}
		die();
    }
	
	/**
     * @param $stateCode
     * @param $countryId
     * @return int
     */
    public function getRegionId($stateCode, $countryId): int
    {
        $regionId = (int)$this->regionFactory->loadByCode($stateCode, $countryId)->getRegionId();
        return $regionId;
    }
	
	/** 
     * Get the list of regions present in the given Country
     * Returns empty array if no regions available for Country
     * 
     * @param String
     * @return Array/Void
    */
    public function getRegionsOfCountry($countryCode) {
        $regionCollection = $this->country->loadByCode($countryCode)->getRegions();
        $regions = $regionCollection->loadData()->toOptionArray(false);
        return $regions;
    }
}