<?php
	namespace Emc\Singleseller\Observer;
	use Magento\Framework\Event\Observer;
	use Magento\Framework\Event\ObserverInterface;
	use Psr\Log\LoggerInterface;
	class AddProductAfter implements ObserverInterface
{

		protected $quote;
		public $logger;
		
		/**
			* @var \Magento\Framework\Stdlib\CookieManagerInterface
		*/
		protected $_cookieManager;
		
		public function __construct(\Magento\Checkout\Model\Session $checkoutSession, LoggerInterface $logger, \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager) {
			$this->quote = $checkoutSession;
			$this->logger = $logger;
			$this->_cookieManager = $cookieManager;
		}
		
		public function execute(\Magento\Framework\Event\Observer $observer) {
			$quote = $this->quote->getQuote();
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$customerSession = $objectManager->create('Magento\Customer\Model\Session');
			$firstname = $lastname = $street = $postal = $city = $region = $country = "";
			$addressData = $cookieArray = [];
			if ($customerSession->isLoggedIn()) {
				$firstname = $customerSession->getCustomer()->getFirstname();
				$lastname = $customerSession->getCustomer()->getLastname();
			}
			$cookieValue = $this->_cookieManager->getCookie(\Emc\Singleseller\Controller\Index\Index::COOKIE_NAME);
			$this->logger->info($cookieValue);
			$cookieArray = json_decode($cookieValue, true);
			if (isset($cookieArray) && is_array($cookieArray) && count($cookieArray) > 0) {
				$street = $cookieArray['street'];
				$postal = $cookieArray['postal'];
				$city = $cookieArray['city'];
				$region = $cookieArray['region'];
				$regionId = $cookieArray['region_id'];
				$country = $cookieArray['country'];
				$street = rtrim(',',$street);
				$addressData = [
                "street" => $street,
                "city" => $city,
                "postcode" => $postal,
                "country_id" => $country,
                "region" => $region,
                "region_id" => $regionId
				];
				if ($firstname != '' && $lastname != '') {
					$addressData['firstname'] = $firstname;
					$addressData['lastname'] = $lastname;
				}
				$quote->getShippingAddress()->addData($addressData);
				$quote->save();
			}
		}
	
	}
		
