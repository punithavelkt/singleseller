<?php

namespace Emc\Singleseller\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class AddShippingAddressInCookie implements ObserverInterface {

    protected $quote;
    public $logger;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    protected $_cookieMetadataFactory;
    protected $_sessionManager;

    public function __construct(
    \Magento\Checkout\Model\Session $checkoutSession, LoggerInterface $logger, \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager, \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory, \Magento\Framework\Session\SessionManagerInterface $sessionManager, \Magento\Directory\Model\CountryFactory $countryFactory
    ) {
        $this->quote = $checkoutSession;
        $this->logger = $logger;
        $this->_cookieManager = $cookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->_sessionManager = $sessionManager;
        $this->_countryFactory = $countryFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $orderShippingAddress = $observer->getOrder()->getShippingAddress();
        $firstname = $lastname = $street = $postal = $city = $region = $country = $latitude = $longitude = "";
        $cookieValue = $this->_cookieManager->getCookie(\Emc\Singleseller\Controller\Index\Index::COOKIE_NAME);
        $this->logger->info("Place Order");
        $this->logger->info($cookieValue);
        $cookieArray = json_decode($cookieValue, true);
        if (isset($cookieArray) && is_array($cookieArray) && count($cookieArray) > 0) {
            $latitude = $cookieArray['latitude'];
            $longitude = $cookieArray['longitude'];
        }
        $street = implode(",", $orderShippingAddress->getStreet());
        $postal = $orderShippingAddress->getPostcode();
        $city = $orderShippingAddress->getCity();
        $region = $orderShippingAddress->getRegion();
        $regionId = $orderShippingAddress->getRegionId();
        $country = $orderShippingAddress->getCountryId();
        $country_name = $this->getCountryname($country);

        $this->logger->info("Street Address");
        $this->logger->info($street);
        $addressData = [
            "street" => $street,
            "city" => $city,
            "postal" => $postal,
            "country" => $country,
            "country_long_name" => $country_name,
            "region" => $region,
            "region_id" => $regionId,
            "firstname" => $orderShippingAddress->getFirstname(),
            "lastname" => $orderShippingAddress->getLastname(),
            "latitude" => $latitude,
            "longitude" => $longitude
        ];
        $this->_cookieManager->deleteCookie(\Emc\Singleseller\Controller\Index\Index::COOKIE_NAME);
        $this->logger->info("Cookie Deleted");
        $this->logger->info(json_encode($addressData));
        $metadata = $this->_cookieMetadataFactory
                ->createPublicCookieMetadata()
                ->setDurationOneYear()
                ->setPath($this->_sessionManager->getCookiePath())
                ->setDomain($this->_sessionManager->getCookieDomain())
                ->setHttpOnly(false);
        $this->_cookieManager->setPublicCookie(
                \Emc\Singleseller\Controller\Index\Index::COOKIE_NAME, json_encode($addressData), $metadata
        );
        $this->logger->info("Cookie Created");
    }

    public function getCountryname($countryCode) {
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }

}
