<?php

namespace Emc\Singleseller\Block;

class GoogleMap extends \Magento\Framework\View\Element\Template
{
    public function getApiKey()
    {
        return $this->_scopeConfig->getValue('address_map/general/addressmap_key');
    }
}