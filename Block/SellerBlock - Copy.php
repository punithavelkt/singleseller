<?php
	
	namespace Emc\Singleseller\Block;
	
	use Emc\SellerSettings\Api\SellerListInterface;
	use Magento\Framework\App\ResourceConnection;
	
	class SellerBlock extends \Magento\Framework\View\Element\Template
	{ 
		protected $_sellerCollectionFactory;
		protected $_storeManager;
		protected $resourceConnection;
 		protected $_scopeConfig;
		protected $_groupCollection;
		protected $sellergroupCollectionFactory;
 		
		public function __construct(
		\Magento\Framework\View\Element\Template\Context $context
		, \Lof\MarketPlace\Model\ResourceModel\Seller\CollectionFactory $sellerCollectionFactory
		, \Magento\Store\Model\StoreManagerInterface $storeManager
		, ResourceConnection $resourceConnection
  		, \Lof\MarketPlace\Model\ResourceModel\Group\Collection $_groupCollection
		, \Emc\SellerGroup\Model\ResourceModel\SellerGroup\CollectionFactory $sellergroupCollectionFactory
		, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
		
		) {
			$this->_sellerCollectionFactory = $sellerCollectionFactory;
			$this->_storeManager = $storeManager;
			$this->resourceConnection = $resourceConnection;
			$this->_scopeConfig = $scopeConfig;
			$this->_groupCollection = $_groupCollection;
			$this->sellergroupCollectionFactory = $sellergroupCollectionFactory;
 			parent::__construct($context);
		}
		
		public function getSellerGroupCollection($sellerArray, $group_id_filter) {
			$groupCollection = $this->sellergroupCollectionFactory->create();
			$groupCollection->addFieldToFilter('seller_id', array('in' => $sellerArray));
			if ($group_id_filter != '') {
				$groupCollection->addFieldToFilter('group_id', $group_id_filter);
			}
			$groupArray = array();
			foreach ($groupCollection as $groupData) {
				$groupArray[$groupData->getSellerId()][] = $groupData->getGroupId();
			}
 			return $groupArray;
		}
		public function getGroupCollection() {
			$groupCollection = $this->_groupCollection;
			$groupArray = array();
			foreach ($groupCollection as $groupData) {
				$groupArray[$groupData->getId()] = $groupData;
			}
			return $groupArray;
		}
		
		public function doDescSort($key) {
			return function ($a, $b) use ($key) {
				return strnatcmp($b[$key], $a[$key]);
			};
		}
		
		 public function getConfig($key)
		{     
			return $this->_scopeConfig->getValue( $key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
		}
		
		
		public function getSellers($latitude,$longitude)
		{
			$sellerSelectFields = array(
            'seller_id', 'name', 'description', 'shopOnOff', 'image','thumbnail','logo_pic','in_poly','url_key'
			);
			$searchResults = array();
			$sellerList = array();
			$shopSettingsTableName = $this->resourceConnection->getTableName('lof_marketplace_seller_settings');
			$deliveryLocationTableName = $this->resourceConnection->getTableName('delivery_map_coordinates');
			$collection = $this->_sellerCollectionFactory->create();
			//$latitude = $longitude = 0;
			$groupCollection = $this->getGroupCollection();
			$group_id_filter = '';
 			$collection->getSelect()
			->joinLeft(
			['ss' => $shopSettingsTableName], "main_table.seller_id = ss.seller_id", [
			"IFNULL(ss.value, '1') AS shopOnOff"
			// ,'shopOnOffx' => 'ss.value'
			]
			);
			$coordinatesCheck = false;
			if ($latitude != '' && $longitude != '' && $latitude != 0 && $longitude != 0) {
				$coordinatesCheck = true;
				 $collection->getSelect()
				->join(
				['dl' => $deliveryLocationTableName], "main_table.seller_id = dl.seller_id", [
				'coordinates' => 'dl.coordinates',
				'in_poly'=>"ST_Contains(dl.polygon,POINT('".$latitude."','".$longitude."'))"
				]
				)->where( " ST_Contains(dl.polygon,POINT('".$latitude."','".$longitude."'))"); 
				
			}
 			//echo $collection->getSelect();
			$resultSellers = array();
			foreach ($collection as $val) {
				$data = $val->getData();
				if (empty($data['shopOnOff'])) {
					$data['shopOnOff'] = 0;
					 
				}
 				$val->setData($data);
				$resultSellers[$data['seller_id']] = $val; // for merging other groups
			}
 			
			// start merging sellers other group
			if (count($resultSellers)) {
				$selerIds = array_keys($resultSellers);
				$sellerGroups = $this->getSellerGroupCollection($selerIds, $group_id_filter);
				if ($sellerGroups) {
					foreach ($sellerGroups as $seller_key => $sellerGroup) {
						foreach ($sellerGroup as $groupId) {
							if (isset($resultSellers[$seller_key])) {
								$searchResults[$groupId][] = $resultSellers[$seller_key];
							}
						}
					}
 				} else{
					foreach ($resultSellers as $resultSeller) {
						$searchResults[$resultSeller['group_id']][] = $resultSeller;
					}
				}
			}
			// end merging sellers other group
			
			foreach ($searchResults as $groupKey => $searchResultGroup) {
				if (isset($groupCollection[$groupKey])) {
					$groupData = array('group_id' => $groupKey, "group_name" => $groupCollection[$groupKey]->getName());
					foreach ($searchResultGroup as $sellerData) {
						$sellerArr = array();
						foreach ($sellerSelectFields as $fields) {
							$sellerArr[$fields] = $sellerData[$fields];
						}
						$groupData['sellerList'][] = $sellerData;
						usort($groupData['sellerList'], $this->doDescSort('shopOnOff'));
					}
					$sellerList[] = $groupData;
				}
			}
			if(count($sellerList)){
				return  ($sellerList);
				}else{
				return  false;
			}
			
			
			
		}
		
	}														