define(
    [
        'uiComponent'
    ],
    function (Component) {

        "use strict";
        var quoteItemData = window.checkoutConfig.quoteItemData;
        return Component.extend({
            defaults: {
                template: 'Emc_Singleseller/summary/item/details'
            },
            quoteItemData: quoteItemData,
            getValue: function(quoteItem) {
                return quoteItem.name;
            },
            getSeller: function(quoteItem) {
                var item = this.getItem(quoteItem.item_id);
                if(item.extension_attributes){
					if(item.extension_attributes.seller_name){
                    	return item.extension_attributes.seller_name;
					}
                }else{
                    return '';
                }
            },
            getItem: function(item_id) {
                var itemElement = null;
                _.each(this.quoteItemData, function(element, index) {
                    if (element.item_id == item_id) {
                        itemElement = element;
                    }
                });
                return itemElement;
            }
        });
    }
);