<?php

namespace Emc\Singleseller\Plugin;

use Magento\Framework\Exception\StateException;
use Psr\Log\LoggerInterface as Logger;

class ShippingInformationManagement {

    protected $cookieManager;

    public function __construct(
    \Magento\Quote\Api\CartRepositoryInterface $quoteRepository, Logger $logger, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    , \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
    , \Emc\DeliveryLocation\Helper\Polygon $polygonHelper
    , \Lof\MarketPlace\Helper\Data $lofHelper
    , \Emc\DeliveryLocation\Model\Maps $deliveryLocation
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->cookieManager = $cookieManager;
        $this->polygonHelper = $polygonHelper;
        $this->lofHelper = $lofHelper;

        $this->deliveryLocation = $deliveryLocation;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
    \Magento\Checkout\Model\ShippingInformationManagement $subject, $cartId, \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {

        $allow_single_seller_product = $this->scopeConfig->getValue('lofmarketplace/seller_settings/allow_single_seller_product', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($allow_single_seller_product) {


            $isDeliverable = false;
            $cookieValue = $this->cookieManager->getCookie(\Emc\Singleseller\Controller\Index\Index::COOKIE_NAME);
            $cookieArray = json_decode($cookieValue, true);
            if (isset($cookieArray) && is_array($cookieArray) && count($cookieArray) > 0) {

                $seller_id = $this->checkSingleSellerQuote($cartId);
                if ($seller_id === false) {
                    throw new StateException(__('Address is invalid.'));
                }
                if ($seller_id === true) {
                    return;
                }


                $quoteShippingAdress = $addressInformation->getShippingAddress();


                $street = $quoteShippingAdress->getData('street');
                $city = $quoteShippingAdress->getData('city');
                $country = $quoteShippingAdress->getData('country_id');
                $region = $quoteShippingAdress->getData('region');
                $address = '';
                if ($street != '') {
                    $address .= $street . " ";
                }
                if ($city != '') {
                    $address .= $city . " ";
                }
                if ($region != '') {
                    $address .= $region . " ";
                }
                if ($country != '') {
                    $address .= $country;
                }
                $latLong = $this->getLatLong($address);
                if ($latLong) {
                    $latitudeX = $latLong['lat'];
                    $longitudeY = $latLong['lng'];
                    $isDeliverable = $this->checkCoordinates($seller_id, $latitudeX, $longitudeY);
                } else {
                    throw new StateException(__('Address is invalid.'));
                }
                if ($isDeliverable) {
                    
                } else {
                    throw new StateException(__('Delivery not available for this location.'));
                }
            }
        }
    }

    public function checkCoordinates($sellerId, $latitudeX, $longitudeY) {

        $isDeliverable = false;
        if (isset($sellerId) && $sellerId >= 0) {
            $point = $latitudeX . ' ' . $longitudeY;
            $deliveryModel = $this->deliveryLocation->getCollection()->addFieldToFilter('seller_id', $sellerId);
            if ($deliveryModel->getSize() > 0) {
                foreach ($deliveryModel as $key => $sellerDetail) {
                    $coOrdinates = json_decode($sellerDetail->getCoordinates());
                    $polygon = array();

                    foreach ($coOrdinates as $coord => $coordValue) {
                        //$this->latitudeArray[] = $coordValue->lat;
                        //  $this->longitudeArray[] = $coordValue->lng;
                        $polygon[] = $coordValue->lat . ' ' . $coordValue->lng;
                    }


                    //  $pointsPolygon = count($this->latitudeArray) - 1;
                    //  $polygonCheck = $this->isInPolgon($pointsPolygon, $this->longitudeArray, $this->latitudeArray, $longitudeY, $latitudeX);
                    $polygonCheck = $this->polygonHelper->pointInPolygon($point, $polygon);
                    if ($polygonCheck) {
                        $isDeliverable = true;
                        break;
                    }
                }
            }
        }
        return $isDeliverable;
    }

    public function checkSingleSellerQuote($cartId) {
        //  $cartId = $cartItem->getQuoteId();
        $quote = $this->quoteRepository->getActive($cartId);
        $cartData = $quote->getAllVisibleItems();
        if (count($cartData) == 0) {
            return true;
        } else {
            $sellerArray = array();
            // get existing sellers
            foreach ($cartData as $item) {
                $seller_id = $this->lofHelper->getSellerIdByProduct($item->getProductId());
                $sellerArray[$seller_id] = $seller_id;
            }
            if (count($sellerArray) > 1) {
                return false;
            } else {
                return $seller_id;
            }
        }
    }

    public function getGoogleAPIKey() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $gkey = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('lofmarketplace/google_api_settings/google_api');
        return $gkey;
    }

    public function getLatLong($address) {
        if ($address == '') {
            return false;
        }
        $key = $this->getGoogleAPIKey();
        $address = str_replace(" ", "+", $address);

        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=" . $key;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        if (isset($response_a->status) && $response_a->status == 'OK') {
            $lat = $response_a->results[0]->geometry->location->lat;
            $long = $response_a->results[0]->geometry->location->lng;
            return array('lat' => $lat, 'lng' => $long);
        }
        return false;
    }

}
