<?php

namespace Emc\Singleseller\Plugin\Checkout\Model\Checkout;

class LayoutProcessor {

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
    \Magento\Checkout\Block\Checkout\LayoutProcessor $subject, array $jsLayout
    ) {
        $street = $postal = $city = $region = $country = $regionId = "";
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');

        $quote = $cart->getQuote();
        // This will return the current quote
        $quoteId = $quote->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quoteFactory = $objectManager->create('\Magento\Quote\Model\QuoteFactory');
        $quote = $quoteFactory->create()->load($quoteId);
        $shippingAddress = $quote->getShippingAddress();
        $street = implode(",", $shippingAddress->getStreet());
        $postal = $shippingAddress->getPostcode();
        $city = $shippingAddress->getCity();
        $region = $shippingAddress->getRegion();
        $regionId = $shippingAddress->getRegionId();
        $country = $shippingAddress->getCountryId();
 
          $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
          $cookieManager = $objectManager->create('Magento\Framework\Stdlib\CookieManagerInterface');
          $cookieArray = [];
          $region = '';
          $cookieValue = $cookieManager->getCookie(\Emc\Singleseller\Controller\Index\Index::COOKIE_NAME);
          $cookieArray = json_decode($cookieValue, true);
          if(isset($cookieArray) && is_array($cookieArray) && count($cookieArray) > 0) {
          $street = $cookieArray['street'];
          $postal = $cookieArray['postal'];
          $city   = $cookieArray['city'];
          $region = $cookieArray['region'];
          $regionId = $cookieArray['region_id'];
          $country = $cookieArray['country'];
          }
          

         

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['street']['children'][0]['value'] = $street;

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['region_id']['value'] = $regionId;

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['region']['value'] = $region;

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['city']['value'] = $city;


        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['postcode']['value'] = $postal;

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['country_id']['value'] = $country;

        

        return $jsLayout;
    }

}
