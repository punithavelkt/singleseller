<?php

namespace Emc\Singleseller\Plugin\Checkout\Model;

class ConfigProviderPlugin extends \Magento\Framework\Model\AbstractModel {

    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result) {
        $product_seller = array();
        $items = $result['totalsData']['items'];

        for ($i = 0; $i < count($items); $i++) {

            $sellerName = '';
            $productName = $items[$i]['name'];
            if (isset($items[$i]['extension_attributes']) && isset($items[$i]['extension_attributes']['seller_name'])) {
                $sellerName = $items[$i]['extension_attributes']['seller_name'];
            }
            $items[$i]['seller_name'] = $sellerName;
            $product_seller[$items[$i]['item_id']] = $sellerName;
        }

        $quoteItemData = $result['quoteItemData'];
        for ($i = 0; $i < count($quoteItemData); $i++) {

            $item_id = $quoteItemData[$i]['item_id'];
            if (isset($product_seller[$item_id])) {
                $quoteItemData[$i]['seller_name'] =  $product_seller[$item_id] ;
                $quoteItemData[$i]['product']['seller_name'] =   $product_seller[$item_id];
            }
        }
        $result['quoteItemData'] = $quoteItemData;
        $result['totalsData']['items'] = $items;
        return $result;
    }

}
