<?php

namespace Emc\Singleseller\Plugin\Cart;

use Psr\Log\LoggerInterface;
use Magento\Quote\Api\CartRepositoryInterface;

class Cart {

    /**
     * @var Psr\Log\LoggerInterface;
     */
    public $logger;
    protected $scopeConfigInterface;
    protected $quoteRepository;
    protected $helper;
    protected $productRepository;
    protected $_messageManager;

    public function __construct(
    LoggerInterface $logger
    , \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
    , CartRepositoryInterface $quoteRepository
    , \Lof\MarketPlace\Helper\Data $helper
    , \Magento\Catalog\Model\ProductRepository $productRepository
    , \Magento\Framework\Message\ManagerInterface $messageManager
            ,\Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->logger = $logger;
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->quoteRepository = $quoteRepository;
        $this->helper = $helper;
        $this->productRepository = $productRepository;
        $this->_messageManager = $messageManager;
        $this->_checkoutSession = $checkoutSession;

    }

    /**
     * Plugin to check sigle seller and restrict
     * @param \Magento\Quote\Model\Quote\Item\Repository $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Api\Data\CartItemInterface $cartItem
     * @return string
     */
    public function aroundAddProduct(
    \Magento\Checkout\Model\Cart $subject, \Closure $proceed, $productInfo, $requestInfo) {

        $this->logger->info("aroundAddProduct Plugin Started");

        $quote = $this->_checkoutSession->getQuote();
       
        if ($this->checkSingleSellerQuote($quote->getId(), $productInfo)) {
           $response = $proceed($productInfo, $requestInfo);
        } else {
            $this->logger->info("aroundAddProduct Single seller error");
            $response['error'] = array('staus' => false, 'mesage' => __('Your cart contains items from a different Store. Would you like to remove other store products before adding this store product?'));
            $message = __('Your cart contains items from a different Store. Would you like to remove other store products before adding this store product?');
            $this->_messageManager->addErrorMessage(
                    $message
            );
        }
        $this->logger->info("aroundAddProduct Plugin Ends");

        return $response;
    }

    public function checkSingleSellerQuote($cartId, $productInfo) {
        $allow_single_seller_product = $this->scopeConfigInterface->getValue('lofmarketplace/seller_settings/allow_single_seller_product', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        if ($allow_single_seller_product) {
            //  $cartId = $cartItem->getQuoteId();
            $quote = $this->quoteRepository->getActive($cartId);
            $cartData = $quote->getAllVisibleItems();
            if (count($cartData) == 0) {
                return true;
            } else {
                $sellerArray = array();
                // get existing sellers
                foreach ($cartData as $item) {
                    $seller_id = $this->helper->getSellerIdByProduct($item->getProductId());
                    $sellerArray[$seller_id] = $seller_id;
                }
                // get current product seller
                $currentSku = $productInfo->getSku();
                if ($currentSku) {
                    $product = $this->productRepository->get($currentSku);
                    $currentProductSeller = $product->getData('seller_id');
                    $sellerArray[$currentProductSeller] = $currentProductSeller;
                }
                if (count($sellerArray) > 1) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        return true;
    }

}
